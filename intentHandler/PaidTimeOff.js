
var Message = require('../Message');
var dialogHandler = require('../dialogHandler');
var format = require("string-template");
var compile = require("string-template/compile");
var regexp = require('node-regexp');
var asynccall = false;
var timeOff = require('../services/workDay/TimeOff');
var vacation_left = compile(
    `Your leave has been applied successfully.
`);


var vacation_used = compile(
    ` Nothing Left
`
);

var default_msg = compile(`
<p>
Sorry {0}, I could not complete the operation. I will try it later.  
</p>
`);

var re_left = regexp()
    .either('sick leave', 'tomorrow')
    .ignoreCase()
    .toRegExp()



module.exports = {
    
    init : function (req, res) {
            var intent_tree = req.session.intent_tree;
            var testMessage = intent_tree.user_text;
            var msg_out = "";

            var left = re_left.test(testMessage);            

            if(left){
                asynccall = true;
                timeOff.AdjustTimeOff("Colin Drake","21035","2018-06-15","8","1D-PTO Plan","JR Sick",function(output)
                {   
                    console.log("Method Called ");                  
                    msg_out = vacation_left();    

                    dialogHandler.defaultDialog.init(msg_out,req,res);                                      
                });                        

            }else{
                msg_out = default_msg(req.session.userContext.user_name);
            }
            if(!asynccall)
                dialogHandler.defaultDialog.init(msg_out, req, res );
    }
}

