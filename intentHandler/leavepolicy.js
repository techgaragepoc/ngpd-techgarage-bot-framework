var Message = require('../Message');
var dialogHandler = require('../dialogHandler');
var format = require("string-template");
var compile = require("string-template/compile");
var regexp = require('node-regexp');

var loa = compile(
    `We understand you there are times when you may need a leave of absence. Choose one of the following to learn more:

<ul>
    <li><a class="change-font" href="#">View our full Leave of Absence Policy</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Number of Sick Days">Number of Sick Days</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Adoption Policy">Adoption Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Bereavement Policy">Bereavement Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align"  id="Sabbatical Policy">Sabbatical Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Long Term Disability">Long Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Schedule Your Leave of Absence">Schedule Your Leave of Absence</a>
</div>
`);

var loa_schedule = compile(
`
When you\'re ready to schedule your leave of absence contact the HR department at <a href="#" class="change-font">555-145-7332</a><br>

<br>
`
);

var flp = compile(
    `Here's a summary of our family leave policy:
<ul>
<li>8 weeks of paid maternity leave</li>
<li>2 weeks of paid paternity leave</li>
<li>2 weeks of paid adotion leave</li>
<li>10 days (consecutive or non-consecutive) of spousal or elder care leave</li>
</ul>
<br>
<ul>
    <li><a class="change-font" href="#">View our full family leave policy</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a> to request family leave</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Leave of Absence">Leave of Absence</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Adoption Policy">Adoption Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Sick Days Remaining">Sick Days Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Long Term Disability">Long Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Schedule Your Leave of Absence">Schedule Your Leave of Absence</a>
</div>
`);

var adop = compile(
    `Your family is growing! We provide 2 weeks of paid adoption leave.
<br>
<ul>
    <li><a class="change-font" href="#">View our full adoption policy</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a> to request adoption leave</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
</div>
`);

var adop_ben = 'We provide 2 weeks of paid adoption leave in addition to other support and resources.<br>';
adop_ben += '<ul><li><a href="#">View our full adoption policy</a></li><li>Call <a href="#">555-398-0245</a> to request adoption leave.</a></ul><br>';
adop_ben += '<br><div class="chat-footer">Related topics: <br>';
adop_ben += '<hr class="hr-topics">';
adop_ben += '<a href="#" onclick="hello(this)" class="anchor-align" id="Schedule Your Leave of Absence">Schedule Your Leave of Absence</a>';
adop_ben += '<a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>';
adop_ben += '<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>';
adop_ben += '<a href="#" onclick="hello(this)" class="anchor-align" id="Number of Sick Days">Number of Sick Days</a>';
adop_ben += '</div>';

var adop_yes = compile(
    `Yes! We provide 2 weeks of paid adoption leave.
<br>
<ul>
    <li><a class="change-font" href="#">View our full adoption policy</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a> to request adoption leave</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Adoption Benefits">Adoption Benefits</a>
</div>
`);

var bereave = compile(
    `The death of a loved one is always difficult. We provide 1 week of bereavement leave for the death of a spouse and 3 days for the death of a family member.
<br>
<ul>
    <li><a class="change-font" href="#">View our full bereavement policy</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a> to request bereavement leave</a></li>
    <li><a class="change-font" href="#">Contact the Employee Assistance Program</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Leave of Absence">Leave of Absence</a>
</div>
`);

var bereave_spouse = compile(
    `Yes, we provide 1 week of bereavement for the death of a spouse.
<br>
<ul>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a> to request bereavement leave</a></li>
    <li><a class="change-font" href="#">View our full bereavement policy</a></li>
    <li><a class="change-font" href="#">Contact the Employee Assistance Program</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Leave of Absence">Leave of Absence</a>
</div>
`);

var bereave_family = compile(
    `Yes, we provide 3 days of bereavement for the dealth of a family member
<br>
<ul>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a> to request bereavement leave</a></li>
    <li><a class="change-font" href="#">View our full bereavement policy</a></li>
    <li><a class="change-font" href="#">Contact the Employee Assistance Program</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Leave of Absence">Leave of Absence</a>
</div>
`);

var sabbatical_yes = compile(
    `Yes, you can take a sabbatical. You currently qualify for a 4 week unpaid sabbatical.

<br>
<ul>
    <li><a class="change-font" href="#">View our full sabbatical policy</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a> to sabbatical leave</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Number of Sick Days">Number of Sick Days</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Leave of Absence">Leave of Absence</a>
</div>
`);

var sabbatical = compile(
    `We understand that a short-term break from work can be important to your mental and physical health. You currently qualify for a 4 week unpaid sabbatical.

<br>
<ul>
    <li><a class="change-font" href="#">View our full sabbatical policy</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a> to sabbatical leave</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Number of Sick Days">Number of Sick Days</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Leave of Absence">Leave of Absence</a>
</div>
`);


var ltd = compile(
    `You have enrolled in our Long-Term Disability benefit. In the event of a serious health condition that prevents you from preforming the essential functions of your position you may take up to 52 weeks of leave paid at 60% of your salary after exhausting your Short-Term Disability.
<br>
<ul>
    <li><a class="change-font" href="#">View our full Long-Term Disability</a></li>
    <li><a class="change-font" href="#">Adjust your Long Term Disability coverage</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a>  to talk to an HR rep</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Sabbatical Policy">Sabbatical Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Leave of Absence">Leave of Absence</a>
</div>
`);
var ltd_yes = compile(
    `Yes, you have Long-Term Disability.
<br>
<ul>
    <li><a class="change-font" href="#">View our full Long-Term Disability</a></li>
    <li><a class="change-font" href="#">Adjust your Long Term Disability coverage</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a>  to talk to an HR rep</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Sabbatical Policy">Sabbatical Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Leave of Absence">Leave of Absence</a>
</div>
`);

var std = compile(
    `We offer Short-Term Disability, but it doesn't look like you enrolled. Enrollment for this program opens November 7 for the following benefit year.
<br>
<ul>
    <li><a class="change-font" href="#">View our full Short Term Disability policy</a></li>
    <li><a class="change-font" href="#">Adjust your Short Term Disability coverage</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a>  to talk to an HR rep</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Sabbatical Policy">Sabbatical Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Long Term Disability">Long Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Leave of Absence">Leave of Absence</a>
</div>
`);

var std_yes = compile(
    `Yes, we offer Short Term Disability.
<br>
<ul>
    <li><a class="change-font" href="#">View our full Short Term Disability policy</a></li>
    <li><a class="change-font" href="#">Adjust your Short Term Disability coverage</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a>  to talk to an HR rep</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Sabbatical Policy">Sabbatical Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Long Term Disability">Long Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Leave of Absence">Leave of Absence</a>
</div>
`);
var default_msg = compile(
`
<p>
Sorry {0}, I may not have the information you need. At the moment I only know how to answer questions related to sick leave, vacation and time off policies. I’m not familiar with your question but I will do my best to learn about this topic later. 
</p>
`);

var child_maternity_benefits = compile(`Maternity leave is covered under Short Term Disability insurance. You\'ll continue to be paid on normal payroll cycles while you are away.<br>
<ul><li><a href="#" id="Review Our Leave Policy" class ="change-font">Review Our Leave Policy</a></li></ul><br>

<div class ="chat-footer">
    Related topics: <br>
    <hr class ="hr-topics">
    <a href="#" onclick="hello(this)" class ="anchor-align" id="Schedule Your Leave of Absence">Schedule Your Leave of Absence</a>
    <a href="#" onclick="hello(this)" class ="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
    <a href="#" onclick="hello(this)" class ="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
    <a href="#" onclick="hello(this)" class ="anchor-align" id="Number of Sick Days">Number of Sick Days</a>
    <a href="#" onclick="hello(this)" class ="anchor-align" id="Sick Days Remaining">Sick Days Remaining</a>
</div>`);

var child_maternity_leave = compile(`We provide 8 weeks of paid maternity leave.<br>
            <ul>
                <li><a href="#" class ="change-font">View our full family leave policy</a></li>
                <li>Call <a href="#" class ="change-font">555-235-1256</a> to request family leave.</li>
            </ul><br>

            <div class ="chat-footer">
                Related topics: <br>

                <hr class ="hr-topics">
                <a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
                <a href="#" onclick="hello(this)" class="anchor-align" id="Schedule Your Leave of Absence">Schedule Your Leave of Absence</a>
                <a href="#" onclick="hello(this)" class ="anchor-align last" id="Vacation Time Remaining">Vacation Time Remaining</a>
            </div>
        `);

var child_paternity_leave = compile(`We provide 2 weeks of paid paternity leave..<br>
            <ul>
                <li><a href="#" class ="change-font">View our full family leave policy</a></li>
                <li>Call <a href="#" class ="change-font">555-235-1256</a> to request family leave.</li>
            </ul><br>

            <div class ="chat-footer">
                Related topics: <br>

                <hr class ="hr-topics">
                <a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
                <a href="#" onclick="hello(this)" class="anchor-align" id="Schedule Your Leave of Absence">Schedule Your Leave of Absence</a>
                <a href="#" onclick="hello(this)" class="anchor-align" id="Adoption Benefits">Adoption Benefits</a>
                <a href="#" onclick="hello(this)" class ="anchor-align last" id="Vacation Time Remaining">Vacation Time Remaining</a>
            </div>
    `);

var policy_options = compile(
    `Currently, I’m trained to provide information around policies listed below.
<br>
<div class="chat-footer">
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Sabbatical Policy">Sabbatical Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Leave of Absence">Leave of Absence</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Number of Sick Days">Number of Sick Days</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Adoption Policy">Adoption Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Bereavement Policy">Bereavement Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Long Term Disability">Long Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Schedule Your Leave of Absence">Schedule Your Leave of Absence</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Child Maternity Leave">Maternity Leave</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Child Magternity Benefits">Maternity Benefits</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Child Paternity Leave">Paternity Leave</a>

</div>
`);



        var re_ques = regexp()
                    .either('get', 'take',  'do we', 'can i', 'can we' )
                    .ignoreCase()
                    .toRegExp()
        var re_family = regexp()
                    .either('family', 'parent', 'mother', 'father', 'grandmother', 'grandfather', 'grandparent', 'sister', 'brother')
                    .ignoreCase()
                    .toRegExp()
        var re_spouse = regexp()
                    .either('wife', 'husband', 'spouse', 'spousal')
                    .ignoreCase()
                    .toRegExp()
        var re_loa = regexp()
                    .either('absence', 'stop work', 'stop working/;/', 'reserve', 'substance', 'rehab', 'surgery', 'back to school', 'back to college', 'military', 'armed guard')
                    .ignoreCase()
                    .toRegExp()
        var re_loa_schedule = regexp()
                    .either('schedule', 'plan', 'request')
                    .ignoreCase()
                    .toRegExp()
        var re_flp = regexp()
                    .either('family')
                    .ignoreCase()
                    .toRegExp()

        var re_child_maternity_benefits = regexp()
            .either('maternity benefits', 'maternity paycheck', 'paid maternity')
            .ignoreCase()
            .toRegExp()

        var re_child_maternity_leave = regexp()
                                       .either('maternity timeoff', 'maternity leave', 'maternity leave of absence', 'maternity loa', 'maternity')
                                       .ignoreCase()
                                       .toRegExp()

        var re_child_paternity_leave = regexp()
                                       .either('paternity timeoff', 'paternity leave', 'paternity leave of absence', 'paternity loa', 'new dad', 'new father', 'paternity')
                                       .ignoreCase()
                                       .toRegExp()

        var re_child_529 = regexp()
            .either('529', 'college savings', 'kids education', 'college plan', 'education')
            .ignoreCase()
            .toRegExp()

        var re_coverage = regexp()
            .either('coverage', 'benefits', 'health', 'dental', 'vision', 'lifeinsurance', 'life insurance')
            .ignoreCase()
            .toRegExp()

        var re_adop = regexp()
                    .either('adoption', 'adopting', 'adopt', 'adopted')
                    .ignoreCase()
                    .toRegExp()
        var re_adop_kid = regexp()
                    .either('kid', 'child', 'baby', 'benefits')
                    .ignoreCase()
                    .toRegExp()

        var re_bereave = regexp()
                    .either('Bereavement', 'death', 'funeral', 'died', 'dies')
                    .ignoreCase()
                    .toRegExp()
        var re_sabbatical = regexp()
                    .either('sabbatical', 'unpaid', 'break', 'sabbaticals')
                    .ignoreCase()
                    .toRegExp()
        var re_disability = regexp()
                    .either('disability', 'disable', 'hurt', 'injured', "can't work", 'unable to work', 'std', 'ltd')
                    .ignoreCase()
                    .toRegExp()
        var re_ltd = regexp()
                    .either("can't work", 'unable to work', 'ltd', 'long term', 'long-term')
                    .ignoreCase()
                    .toRegExp()



module.exports = {

    init : function (req, res) {
        var intent_tree = req.session.intent_tree;
        var defaultDialog = true;
        var msg_out = "";
        testMessage = intent_tree.user_text;
      
        var _loa = re_loa.test(testMessage); //re_loa.test(intent_tree.verb) || re_loa.test(intent_tree.noun);
        var _loa_schedule = re_loa_schedule.test(testMessage); //re_loa_schedule.test(intent_tree.verb) || re_loa_schedule.test(intent_tree.noun);
        var _bereave = re_bereave.test(testMessage); //re_bereave.test(intent_tree.verb) || re_bereave.test(intent_tree.noun);
        var _family = re_family.test(testMessage); //re_family.test(intent_tree.verb) || re_family.test(intent_tree.noun);
        var _spouse = re_spouse.test(testMessage); //re_spouse.test(intent_tree.verb) || re_spouse.test(intent_tree.noun);
        var _flp = re_flp.test(testMessage); //re_flp.test(intent_tree.verb) || re_flp.test(intent_tree.noun);
        var _child_maternity_benefits = re_child_maternity_benefits.test(testMessage); //re_child_maternity_benefits.test(intent_tree.verb) || re_child_maternity_benefits.test(intent_tree.noun);
        var _child_maternity_leave = re_child_maternity_leave.test(testMessage); //re_child_maternity_leave.test(intent_tree.verb) || re_child_maternity_leave.test(intent_tree.noun);
        var _child_paternity_leave = re_child_paternity_leave.test(testMessage); //re_child_paternity_leave.test(intent_tree.verb) || re_child_paternity_leave.test(intent_tree.noun);
        var _adop = re_adop.test(testMessage); //re_adop.test(intent_tree.verb) || re_adop.test(intent_tree.noun);
        var _adop_kids = re_adop_kid.test(testMessage); //re_adop_kid.test(intent_tree.verb) || re_adop_kid.test(intent_tree.noun);
        var _ques =  re_ques.test(testMessage); //re_ques.test(intent_tree.verb) || re_ques.test(intent_tree.noun);
        var _sabbatical = re_sabbatical.test(testMessage); //re_sabbatical.test(intent_tree.verb) || re_sabbatical.test(intent_tree.noun);
        var _disability = re_disability.test(testMessage); //re_disability.test(intent_tree.verb) || re_disability.test(intent_tree.noun);
        var _ltd = re_ltd.test(testMessage); //re_ltd.test(intent_tree.verb) || re_ltd.test(intent_tree.noun);

        if (_loa) {
            if (_loa_schedule) {
                msg_out = loa_schedule();
            } else {
                msg_out = loa();
            }

        } else if (_bereave) {
            if (_family) {
                msg_out = bereave_family();
            } else if (_spouse) {
                msg_out = bereave_spouse();
            } else {
                msg_out = bereave();
            }
        } else if (_flp) {
            msg_out = flp();
        }
        else if (_child_maternity_benefits) {
            msg_out = child_maternity_benefits();
        }
        else if (_child_maternity_leave) {
            msg_out = child_maternity_leave();
        }
        else if (_child_paternity_leave) {
            msg_out = child_paternity_leave();
        }
        else if (_adop) {
            if (_adop_kids) {
                msg_out = adop_ben;
            } else if (_ques) {
                msg_out = adop_yes();
            } else {
                msg_out = adop();
            }
        } else if (_bereave) {
            if (_family) {
                msg_out = bereave_family();
            } else if (_spouse) {
                msg_out = bereave_spouse();
            } else {
                msg_out = bereave();
            }
        } else if (_sabbatical) {
            if (_ques) {
                msg_out = sabbatical_yes();
            } else {
                msg_out = sabbatical();
            }
        } else if (_disability) {
            if (_ltd) {
                if (_ques) {
                    msg_out = ltd_yes();
                } else {
                    msg_out = ltd();
                }
            } else {
                if (_ques) {
                    msg_out = std_yes();
                } else {
                    msg_out = std();
                }
            }
        } else {
            msg_out = policy_options();
        }
        if(defaultDialog)
            dialogHandler.defaultDialog.init(msg_out, req, res);
    }
};