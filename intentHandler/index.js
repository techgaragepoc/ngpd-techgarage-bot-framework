var Personality = require('./Personality'),
    intentProcessor = require('./intentProcessor');

module.exports = {
    Personality,    
    intentProcessor
}