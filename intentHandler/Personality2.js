
var Message = require('../Message');
var dialogHandler = require('../dialogHandler');
var format = require("string-template");
var compile = require("string-template/compile");

var Address_template = compile(`<p> Just to clarify, did you mean Home Address or Office Address? </p>
    <div>
        <a href="#" onclick="hello(this)"  id="Home">Home Address</a>
        <a href="#" onclick="hello(this)"  id="Office">Office Address</a>
    </div>
`);

module.exports = {

    init : function(req, res, intent_tree){
        var Name, Address;
        intent_tree.entities.forEach(function(element) {
            if(element.entity_type === "Name")
                Name = element
        }, this);
        intent_tree.entities.forEach(function(element) {
            if(element.entity_type === "Address")
                Address = element
        }, this);

        if(Name){
            dialogHandler.defaultDialog.init("My name is Wanda", req, res);
        }else if(Address){
            if(req.session.dialogContext.newconversation){
                dialogHandler.chainDialog.init(Address_template(), intent_tree.intent, "Second", req, res);
            }else{
                switch (req.session.dialogContext.next_step){
                    case "Second": 
                        var response = req.session.messageContext.text;
                        if(response == "Home"){
                            dialogHandler.defaultDialog.init("My Home address is X-Home", req, res );
                        }else{
                            dialogHandler.defaultDialog.init( "My Home address is X-Office", req, res);
                        }
                        break;
                    default:
                        dialogHandler.defaultDialog.init(req, res, "Could not understand");
                }
            }
        }else{
             msg = "Could not understand!!";
            dialogHandler.Personality.defaultDialog.init(msg, req, res);
        }

    }

}