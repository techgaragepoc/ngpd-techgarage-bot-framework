
var Message = require('../Message');
var dialogHandler = require('../dialogHandler');
var format = require("string-template");
var compile = require("string-template/compile");
var regexp = require('node-regexp');
var asynccall = false;
var timeOff = require('../services/workDay/TimeOff');
var holidays = compile(`Here is the holiday list  for 2018 {0}  `);

var vacation_used = compile('Hello'
);

var default_msg = `<p>
Sorry {0}, I may not have the information you need. Right now I can only answer questions related to paid time off, help you find answers 
around company benefits, questions related to pregnancy policy, or assist with payroll information, like paycheck details and schedules. 
<br>
Please visit our <a href="#">Customer Care Page</a> for details on how to reach us or call our toll free customer care number 1800-253-4276
</p>
`;

var re_left = regexp()
    .either('calendar', 'year', 'need', 'remain', 'pending', 'get')
    .ignoreCase()
    .toRegExp()

module.exports = {

    init: function (req, res) {
        var intent_tree = req.session.intent_tree;

        var msg_out = "";

        var left = re_left.test(intent_tree.verb) || re_left.test(intent_tree.noun);

        if (left) {
            asynccall = true;
            console.log(" reached here");
            timeOff.GetHolidayCalendar("Holidays for the United States of America", function (result) {
                
                var x = '  <hr class="hr-topics">  '
                var list = x;
                
                var output = JSON.parse(result);
                console.log(" also reached here " + output);

                for (j = 0; j < output.Vacation.length; j++) {
                    list = list + output.Vacation[j].Name + x;
                }

                msg_out = holidays(list);
                dialogHandler.defaultDialog.init(msg_out, req, res);
            });

        } else {
            msg_out = default_msg(req.session.userContext.user_name);
        }
        if (!asynccall)
            dialogHandler.defaultDialog.init(msg_out, req, res);
    }
}

