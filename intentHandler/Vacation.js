
var Message = require('../Message');
var dialogHandler = require('../dialogHandler');
var format = require("string-template");
var compile = require("string-template/compile");
var regexp = require('node-regexp');
var asynccall = false;
var timeOff = require('../services/workDay/TimeOff');
var vacation_left = compile(
    `Here is your balance time off for each plan<p> {0}
     <p>
Note: Vacation time can only be taken in full day increments.<br><br>
<ul>
    <li> <a class="change-font" href="#">Read our vacaction policy</a> </li> 
    <li> <a class="change-font" href="#">Request vacation time</a> </li> 
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Sick Days">Sick Days</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="stop working">Leave of Absence</a>
<a href="#" onclick="hello(this)" class="anchor-align"  id="Sabbatical Policy">Sabbatical Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Long Term Disability">Long Term Disability</a>
</div>
`);


var vacation_used = compile(
    `You've used none of your 17 vacation days available this year.

Note: Vacation time can only be taken in full day increments.<br>
<a class="change-font" href="#">Read our vacaction policy</a> | <a href="#">Request vacation time</a> | <a href="#">Employment law</a>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Sick Days">Sick Days</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Leave of Absence">Leave of Absence</a>
<a href="#" onclick="hello(this)" class="anchor-align"  id="Sabbatical Policy">Sabbatical Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Long Term Disability">Long Term Disability</a>
</div>
`
);

var default_msg = compile(`
<p>
Sorry {0}, I may not have the information you need. At the moment I only know how to answer questions related to sick leave, vacation and time off policies. I’m not familiar with your question but I will do my best to learn about this topic later. 
</p>
`);

var re_left = regexp()
    .either('left', 'request', 'need', 'remain','pending', 'do i have', 'i have','Time Off balances')
    .ignoreCase()
    .toRegExp()

var re_used = regexp()
            .either('used', 'carry over', 'next', 'put', 'approved', 'buy', 'consume', 'avail', 'utilize')
            .ignoreCase()
            .toRegExp()

module.exports = {
    
    init : function (req, res) {
            //console.log('vacation hit');
            var intent_tree = req.session.intent_tree;
            var testMessage = intent_tree.user_text;
            var msg_out = "";

            var left = re_left.test(testMessage);
            var used = re_used.test(testMessage);

            if(left){
                //console.log('left true');
                asynccall = true;
                //console.log(new Date());
                timeOff.GetTimeOffPlanBalances(21035, null, 'Network_Telecom_supervisory',function(output)
                {                    
                    //console.log(new Date());
                   //console.log('Workday API Output:', output);
                   output = JSON.parse(output);
                   
                   msg_out = default_msg(req.session.userContext.user_name);

                   if(output.Vacation !== undefined && output.Vacation.length>=1)
                   {
                        var x = '  <hr class="hr-topics">  '
                        var list = x;
                       for(i=0;i<output.Vacation.length;i++)
                       {
                            //list = list + output.Vacation[i].Name + ": <b>" + output.Vacation[i].Balance + "</b>" + x;
                            list = list + output.Vacation[i].Description + ": <b>" + output.Vacation[i].Balance + "</b>" + x;
                       }
                       console.log ("List " + list);
                        msg_out = vacation_left(list);                                        
                   }
                                 
                    dialogHandler.defaultDialog.init(msg_out, req, res );                                      
                });        
                
            }else if(used){
                msg_out = vacation_used();
            }else{
                msg_out = default_msg(req.session.userContext.user_name);
            }
            //console.log('Async', asynccall);
            if(!asynccall)
                dialogHandler.defaultDialog.init(msg_out, req, res );
    }
}

