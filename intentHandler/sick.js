
var Message = require('../Message');
var dialogHandler = require('../dialogHandler');
var format = require("string-template");
var compile = require("string-template/compile");
var regexp = require('node-regexp');
var synoperation = false;
var dateFormat = require('dateformat');

var sick = compile(
    `All workers are granted 40 hours of Sick time each year. If you don't use them all they do not carry over to the next year.

<ul>
    <li><a class="change-font" href="#">Sick time policy</a></li>
    <li><a class="change-font" href="#">Report sick time</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="stop working">Leave of Absence</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Long Term Disability">Long Term Disability</a>
</div>
`);

var sick_remaining = compile(
    `Hope you're feeling okay. If not, you still have 3 of your 5 sick days left to use this year. If you don't use them all they do not carry over to the next year.
    <ul>
    <li>Report sick time </li>
    </ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Remaining">Vacation Time Remaining</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="stop working">Leave of Absence</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
<a href="#" onclick="hello(this)" class="anchor-align last" id="Long Term Disability">Long Term Disability</a>
</div>
`);


var elder_care = compile(
    `We offer 10 days of consecutive or non-consecutive elder care leave.
<br>
<ul>
    <li><a class="change-font" href="#">View our full family leave policy</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a> to request family leave</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Used">Vacation Time Used</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
</div>
`);

var spousal_care = compile(
    `We offer 10 days of consecutive or non-consecutive spousal care leave.
<br>
<ul>
    <li><a class="change-font" href="#">View our full family leave policy</a></li>
    <li><a class="change-font" href="#">Call <a href="#">555-235-1256</a> to request family leave</a></li>
</ul>
<br>
<div class="chat-footer">
Related topics: <br>
<hr class="hr-topics">
<a href="#" onclick="hello(this)" class="anchor-align" id="Family Leave Policy">Family Leave Policy</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Vacation Time Used">Vacation Time Used</a>
<a href="#" onclick="hello(this)" class="anchor-align" id="Short Term Disability">Short Term Disability</a>
</div>
`);

var mark_sick_time = compile(
    `Your request has been submitted successfully for {0}.
`);


var default_msg = compile(`
<p>
Sorry {0}, I may not have the information you need. At the moment I only know how to answer questions related to sick leave, vacation and time off policies. I’m not familiar with your question but I will do my best to learn about this topic later. 
</p>
`);

var re_elder = regexp()
            .either('parent', 'mom', 'dad', 'elder')
            .ignoreCase()
            .toRegExp() 
var re_spouse = regexp()
            .either('spouse', 'wife', 'husband', 'spousal')
            .ignoreCase()
            .toRegExp()                              
var re_request = regexp()
            .either('left', 'take', 'get', 'need', 'request', 'remaining')
            .ignoreCase()
            .toRegExp()
var re_sick = regexp()
            .either('Sick Time', 'now well')
            .ignoreCase()
            .toRegExp()

var re_mark_sick_time = regexp()
            .either('mark')
            .ignoreCase()
            .toRegExp()

module.exports = {
    
    init : function (req, res) {
        
            var intent_tree = req.session.intent_tree;
            var testMessage = intent_tree.user_text;
            var msg_out = "";

            var elder = re_elder.test(testMessage);
            var spouse = re_spouse.test(testMessage);
            var request = re_request.test(testMessage);
            var _sick = re_sick.test(testMessage);
            var sick_time = re_mark_sick_time.test(testMessage);            
        //if (session.userData && session.userData.username) {
            if(elder){
                msg_out = elder_care();
            }else if(spouse){
                msg_out = spousal_care();
            }else if(request){
                msg_out = sick_remaining();
            }else if(_sick){
                msg_out = sick();
            }else if(sick_time){
                synoperation = true;

                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate()+1);
                formattedDate = dateFormat(tomorrow,"yyyy-mm-dd");
                console.log("Date ", formattedDate);

                timeOff.AdjustTimeOff("Colin Drake","21035",formattedDate,"8","1D-PTO Plan","JR Sick",function(output)
                {   
                    console.log("Method Called ");                  
                    formattedDate = dateFormat(tomorrow,"mmmm dS, yyyy");
                    msg_out = mark_sick_time(formattedDate);
                    
                    dialogHandler.defaultDialog.init(msg_out,req,res);  
                    synoperation = false;                                    
                });       
                
            }else {
                msg_out = default_msg(req.session.userContext.user_name);
            }
            if(!synoperation)
            {
                dialogHandler.defaultDialog.init(msg_out, req, res );
            }
    }
    

};