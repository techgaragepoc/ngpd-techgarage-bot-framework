var format = require("string-template");
var compile = require("string-template/compile");
var regexp = require('node-regexp');
var dialogHandler = require('../dialogHandler');


var age = compile(`<p>Old enough to be answering your questions. </p>`);
var appearance = compile(`<p>Happy and bright! How can i help you?</p>`);
var help = compile(`<div><p>I can answer questions related to sick leave, vacation and leave policies. To make things easier you can also choose from the options below:</p>
        <input type="button" onclick="hello(this)" value="Sick Leave" id="Sick Leave"><br>
        <input type="button" onclick="hello(this)" value="Vacation used" id="Vacation used"><br>
        <input type="button" onclick="hello(this)" value="Leave Policy" id="Leave Policy">
        </div>`);

var language = compile(`<p>At present I can understand and communicate in english only</p>`);
var location = compile(`<p> In this universe and in parallel universes</p>`);
var name = compile(`<p> Hi! My name is Wanda. How can I help you? </p>`);
var state = compile(`<p>Couldn’t be better! How can i help you?</p>`);
var about = compile(`I'm an AI support specialist, also known as a bot. I'm a machine-programmed to assist with your questions. If you ask me simple questions or type in a few key words, I will do my best to answer you. The more questions you ask me, the more I learn!  Right now I can help you with questions related to sick leaves, vacation and questions related to leave policies`);
var ai_specialist = compile(`I'm an AI support specialist, also known as a bot. At the moment I only know how to answer questions related to sick leave, vacation and time off policies. I’m not familiar with your question but I will do my best to learn about this topic later.`);
var default_msg = compile(`
<p>
Sorry {0}, at the moment I only know how to answer questions related to sick leave, vacation and time off policies. I’m not familiar with your question but I will do my best to learn about this topic later.</p>
`);

module.exports = {
    
    init : function (req, res) {
            console.log('Personality hit');
            var intent_tree = req.session.intent_tree;
            var msg_out;
            var message_text = req.session.messageContext.text;
            var re_name = regexp()
                .either('name', 'who are you', 'identity', 'call you')
                .ignoreCase()
                .toRegExp()
            
            var re_age = regexp()
                .either('age ', 'old', 'older', 'years')
                .ignoreCase()
                .toRegExp()
            var re_about = regexp()
                .either('about', 'yourself', 'explain', 'describe')
                .ignoreCase()
                .toRegExp()
            var re_location = regexp()
                .either('location', 'live', 'house', 'address')
                .ignoreCase()
                .toRegExp()

            var re_appearance = regexp()
                .either('how are you', 'look like', 'how r ya', 'how r u', "how're you", 'how are ya', "how're ya"   )
                .ignoreCase()
                .toRegExp()   
                
            var re_help = regexp()
                .either('help', 'question', 'assist', 'answer', 'info', 'can you do', 'can do', 'use')
                .ignoreCase()
                .toRegExp()  
              
            var re_state = regexp()
                .either('mood', 'state')
                .ignoreCase()
                .toRegExp()  

            var re_language = regexp()
                .either('language', 'speak', 'languages', 'understand', 'english', 'spanish', 'french', 'german', 'hindi')
                .ignoreCase()
                .toRegExp() 

            var re_about_yes = regexp()
                .either('bot')
                .ignoreCase()
                .toRegExp()
            var re_ai_specialist = regexp()
                .either('ai specialist', 'ai support specialist', 'ai agent', 'support agent', 'support specialist')
                .ignoreCase()
                .toRegExp()
            var re_hi_hello = regexp()
                .either('hi', 'hello', 'hie')
                .ignoreCase()
                .toRegExp()

            if(re_name.test(message_text)){
                msg_out = name();
            }
            else if(re_location.test(message_text)){
                msg_out = location();
            }      
            else if(re_appearance.test(message_text)){
                msg_out = appearance();
            }   
            else if(re_language.test(message_text)){
                msg_out = language();
            }    
            else if(re_age.test(message_text)){
                msg_out = age();
            }            
            else if(re_help.test(message_text)){
                console.log('help is true');
                msg_out = help();
            }   
            else if(re_state.test(message_text)){
                msg_out = state();
            } 
            else if(re_about_yes.test(message_text)){
                msg_out = about();
            }
            else if(re_ai_specialist.test(message_text)){
                msg_out = ai_specialist();
            }
            else if(re_hi_hello.test(message_text)){
                msg_out = name();
            }
            else if(re_about.test(message_text)){
                msg_out = about();
            }
            else
                msg_out = default_msg(req.session.userContext.user_name);

            dialogHandler.defaultDialog.init(msg_out, req, res );
    }           

};