var Personality = require('./Personality');
var Vacation = require('./Vacation');
var Holiday = require('./Holiday');
var Sick = require('./sick');
var leavepolicy = require('./leavepolicy');
var PaidTimeOff = require('./PaidTimeOff');
var Reply = require('../Message/replyHandler');
var nlp = require('../services/nlp_spacy');
var config = require('../config');
var util = require('util');


function callIntentDialog(req, res, message){
    console.log("Received Intent " + JSON.stringify(req.session.intent_tree));
     switch (req.session.intent_tree.intent){
            case "personality":
                Personality.init(req, res);
                break;
            case "vacation":
                Vacation.init(req, res);                
                break;
            case "holiday":
                Holiday.init(req, res);
                break;
            case "sick":
                Sick.init(req, res);
                break;
            case "leavepolicy":
                leavepolicy.init(req, res);
                break;
            case "MarkTimeOff":
                PaidTimeOff.init(req, res);
            default:
                Reply.SendReply(req, res, util.format(config.get('dialog_intentNotFound_msg'), req.session.userContext.user_name), Message.messageFormat.text);
        }
}

module.exports = {
    processIntent : function(req, res, message){
        if(req.session.dialogContext.newconversation){
            nlp.getIntentTree(message.text.toLowerCase(), function(result){
                //var intent_tree = {intent: "Personality", entities: [{entity_type: "Address", value : ""}]}; // API call to get intent tree
                req.session.intent_tree = result;
                callIntentDialog(req, res, message);
            });
            
        }else{
            callIntentDialog(req, res, message);
        }
       
       
    }
}