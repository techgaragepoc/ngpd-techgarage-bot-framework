var dialogContext = require('../context/dialogContext');
var messageFormat = require('../Message/messageFormat');
var reply = require('../Message/replyHandler');
module.exports = {
    init : function(msg, req, res){
        dialogContext.setDialog(req, "Personality", "defaultDialog", "First", "", true)
        reply.SendReply(req, res, msg, messageFormat.text);
       
    }
}