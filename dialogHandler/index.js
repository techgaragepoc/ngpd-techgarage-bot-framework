var chainDialog = require('./chainDialog');
var defaultDialog = require('./defaultDialog');

module.exports = {
    chainDialog,
    defaultDialog
}