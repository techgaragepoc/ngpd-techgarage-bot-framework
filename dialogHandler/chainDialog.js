var dialogContext = require('../context/dialogContext');
var reply = require('../Message/reply');
var dialog_Type = "chainDialog";
module.exports = {
    init : function( msg, dialog_name, next_step, req, res){
        dialogContext.setDialog(req, dialog_name, dialog_Type, "", next_step, false)
        reply.SendReply(res, msg, Message.messageFormat.text);
    }
}