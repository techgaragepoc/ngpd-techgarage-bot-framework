var http = require('http'),
    https = require("https"),
    url = require('url'),
    express = require('express'),
    bodyParser = require('body-parser'),
    request = require("request"),
    process = require('process');
var soap = require('soap');



module.exports = {
  GetTimeOffPlanBalances: function(employeeId,planId,organizationId,callback) {
        try {

            var request = require('request');
            var apiWSDL = './services/workDay/Absence_Management.wsdl';
            //   var userId = 'ISU_Chatbot@cpsg_dpt1';
            var userId = 'ISU_Absence@cpsg_dpt1';

            //   var password = "PH\\\\_h3rP'98jz%4Y";            

            var password = "PH\\\\_h3rP'98jz%4Y";

            let xml = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bsvc="urn:com.workday/bsvc">
            <soapenv:Header>
                <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                    <wsse:UsernameToken>
                        <wsse:Username>ISU_Chatbot@cpsg_dpt1</wsse:Username>
                        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">PH\\\\_h3rP'98jz%4Y</wsse:Password>
                    </wsse:UsernameToken>
                </wsse:Security>
            </soapenv:Header>
            <soapenv:Body>
                <bsvc:Get_Time_Off_Plan_Balances_Request bsvc:version="v31.0">
                    <bsvc:Request_Criteria>
                        <!--Teresa Serrano-->
                        <bsvc:Employee_Reference bsvc:Descriptor="Colin Drake">
                            <bsvc:ID bsvc:type="Employee_ID">21035</bsvc:ID>
                        </bsvc:Employee_Reference>
                    </bsvc:Request_Criteria>
                    <bsvc:Response_Group>
                        <!--Optional:-->
                        <bsvc:Include_Reference>1</bsvc:Include_Reference>
                        <!--Optional:-->
                        <bsvc:Include_Time_Off_Plan_Balance_Data>1</bsvc:Include_Time_Off_Plan_Balance_Data>
                    </bsvc:Response_Group>
                </bsvc:Get_Time_Off_Plan_Balances_Request>
            </soapenv:Body>
        </soapenv:Envelope>`

        var options = {
            url: 'https://wd2-impl-services1.workday.com/ccx/service/cpsg_dpt1/Absence_Management/v31.0',
            method: 'POST',
            body: xml,
            headers: {
                'Content-Type': 'text/xml;charset=utf-8'
            }
        };
                                                                    
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            var vacation = "{}";
        //    console.log('Raw result ', body);

            var xml2js = require('xml2js');
            var parser = new xml2js.Parser({explicitArray: false, trim: true});
            parser.parseString(body, (err, result) => {

                var record = result['env:Envelope']['env:Body']['wd:Get_Time_Off_Plan_Balances_Response']['wd:Response_Data']['wd:Time_Off_Plan_Balance']['wd:Time_Off_Plan_Balance_Data']['wd:Time_Off_Plan_Balance_Record'];
                vacation = '{"Vacation":[';
                        for (i = 0; i < record.length; i++) {
                                                                                    
                            timeUnit = record[i]['wd:Unit_of_Time_Reference']['wd:ID'][1]['_'];
                            
                            planCode = record[i]['wd:Time_Off_Plan_Reference']['wd:ID'][1]['_'];

                            planDescription =  module.exports.getPlanName(planCode);                                        
                            var balance = record[i]['wd:Time_Off_Plan_Balance_Position_Record']['wd:Time_Off_Plan_Balance'];
                            vacation = vacation + "{" + '"Description": ' + '"' + planDescription + '"' + ',"Name": ' + '"' + planCode + '"' + ',"Balance": ' + '"' + balance + " " + timeUnit + '"' + "}";
                            if (i < record.length - 1) {
                                vacation = vacation + ",";
                            }
                        }
                        vacation = vacation + "]}";
                        console.log("Vacations " ,vacation );                                                               
            
            });
            
            callback(vacation);

        });

        /*let x = (error, response, body) => {
          if (!error && response.statusCode == 200) {
            console.log('Raw result ', body);
            //var xml2js = require('xml2js');
            //var parser = new xml2js.Parser({explicitArray: false, trim: true});
            //parser.parseString(body, (err, result) => {
            //  console.log('JSON result', result);
            //});
          };
          console.log('E', response.statusCode, response.statusMessage);  
          response.status(200).json(body)
        };
        
        request(options,x);*/



        }

        catch (e) {

            console.log(e);

            res.status(500).jsonp({ success: false });

        }

    },

    GetHolidayCalendar: function (countryName, callback) {
        try {

            var apiWSDL = './services/workDay/Human_Resources.wsdl';
            var userId = 'ISU_Absence@cpsg_dpt1';
            var password = "PH\\\\_h3rP'98jz%4Y";
            var response = "";
            var args = { Response_Filter: { As_Of_Effective_Date: { $value: "2018-05-09" }, As_Of_Entry_DateTime: { $value: "2018-05-09" } } };
            var options = {
                endpoint: 'https://wd2-impl-services1.workday.com/ccx/service/cpsg_dpt1/Human_Resources/v31.0'
            }

            soap.createClient(apiWSDL, options, function (err, client) {

                var wsSecurity = new soap.WSSecurity(userId, password);

                client.setSecurity(wsSecurity);

                client.Get_Holiday_Calendars(args, function (err, result) {
                    var vacation = "{" + '"Vacation":[';
                    console.log(" Result " + JSON.stringify(result));
                    if (result.Response_Data !== undefined) {
                        for (i = 0; i < result.Response_Data.Holiday_Calendar.length; i++) {
                            var calendar = result.Response_Data.Holiday_Calendar[i].Holiday_Calendar_Data;
                            if (calendar.Name == "Holidays for the United States of America") {

                                for (j = 0; j < calendar.Holiday_Calendar_Event_Data.length; j++) {
                                    vacation = vacation + '{"Name":"' + calendar.Holiday_Calendar_Event_Data[j].Name + '"}';
                                    if (j < calendar.Holiday_Calendar_Event_Data.length - 1) {
                                        vacation = vacation + ",";
                                    }
                                }
                                break;
                            }
                        }
                        vacation = vacation + "]}"

                    }

                    callback(vacation);
                });
            });

        }
        catch (e) {
            console.log(e);
            res.status(500).json({ success: false });
        }
    },

    AdjustTimeOff: function (employeeName, employeeId, vacationDate, vacationTime, planeName, time_code, callback) {
        try {
            var request = require('request');
            var userName = "ISU_Absence@cpsg_dpt1";
            var password = "PH\\\\_h3rP'98jz%4Y";
            let xml = `<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bsv="urn:com.workday/bsvc">
            <x:Header>
                <bsv:Workday_Common_Header>
                    <bsv:Include_Reference_Descriptors_In_Response>false</bsv:Include_Reference_Descriptors_In_Response>
                </bsv:Workday_Common_Header>
                <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                    <wsse:UsernameToken>
                        <wsse:Username>${userName}</wsse:Username>
                        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">${password}</wsse:Password>
                    </wsse:UsernameToken>
                </wsse:Security>
            </x:Header>
            <x:Body>
                                <bsv:Enter_Time_Off_Request bsv:version="v31.0">
                    <bsv:Enter_Time_Off_Data>
                        <bsv:Worker_Reference bsv:Descriptor="${employeeName}">                    
                            <bsv:ID bsv:type="Employee_ID">${employeeId}</bsv:ID>
                        </bsv:Worker_Reference>
                        <bsv:Enter_Time_Off_Entry_Data>
                            
                    <!--ENTER IN THE DATE HERE-->
                            <bsv:Date>${vacationDate}</bsv:Date>
        
                            <bsv:Requested>${vacationTime}</bsv:Requested>
        
                           <bsv:Time_Off_Reference bsv:Descriptor="${planeName}">     	             
                              <bsv:ID bsv:type="Time_Off_Code">${time_code}</bsv:ID>
                           </bsv:Time_Off_Reference>
        
                    <!--ADD COMMENTS THAT SHOW THE CHATBOT DID THE WORK-->
                            <bsv:Comment>Wandabot created this request!</bsv:Comment>
                        </bsv:Enter_Time_Off_Entry_Data>
                    </bsv:Enter_Time_Off_Data>
                </bsv:Enter_Time_Off_Request>
            </x:Body>
        </x:Envelope>`;
            var options = {
                url: 'https://wd2-impl-services1.workday.com/ccx/service/cpsg_dpt1/Absence_Management/v31.0',
                method: 'POST',
                body: xml,
                headers: {
                    'Content-Type': 'text/xml;charset=utf-8'
                }
            };

            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                var result = { Result: { Status: "Successful" } };
            //    console.log('Raw result ', body);
                console.log('E', response.statusCode, response.statusMessage);
                callback(result);

            });

            /*let x = (error, response, body) => {
              if (!error && response.statusCode == 200) {
                console.log('Raw result ', body);
                //var xml2js = require('xml2js');
                //var parser = new xml2js.Parser({explicitArray: false, trim: true});
                //parser.parseString(body, (err, result) => {
                //  console.log('JSON result', result);
                //});
              };
              console.log('E', response.statusCode, response.statusMessage);  
              response.status(200).json(body)
            };
            
            request(options,x);*/


        }
        catch (e) {
            console.log(" HEllo ");
            console.log(e);
            res.status(500).json({ success: false });
        }
    },

    getPlanName: function(planCode) {
        var planList = JSON.parse('{"Plans":[{"Code":"ABSENCE_PLAN-4-31","Value":"USA Sick Plan (Hourly - CA)"},{"Code":"ABSENCE_PLAN-6-84","Value":"Multiple Jobs TOP"},{"Code":"ABSENCE_PLAN-6-86","Value":"1D-PTO Plan"},{"Code":"ABSENCE_PLAN-6-87","Value":"DM Vacation"},{"Code":"ABSENCE_PLAN-6-88","Value":"JR Vacation Plan"},{"Code":"ABSENCE_PLAN-6-89","Value":"JR Sick Plan"}]}');    
        var planName = "";
        for(j=0;j<planList.Plans.length;j++)
        {
            if(planCode === planList.Plans[j].Code)
            {
                planName = planList.Plans[j].Value;
                break;
            }
            
        }
        return planName;

    }

    /*  AdjustTimeOff: function (employeeId, planId, organizationId, callback) {
          try {
  
              var apiWSDL = './services/workDay/Absence_Management.xml';   
              
                 var userId = 'ISU_Absence@cpsg_dpt1';
  
                 var password = "PH\\\\_h3rP'98jz%4Y";
  
              var response = "";            
           // var args = {"Enter_Time_Off_Data":{"Worker_Reference":{attributes:{"wd:type":"Descriptor"},$value:"Colin Drake","ID":[{"attributes":{"wd:type":"Employee_ID"},"$value":"21035"}]},"Enter_Time_Off_Entry_Data":{"Date":{"$value":"2018-06-09"},"Requested":{"$value":"8"},"Time_Off_Reference":{attributes:{"wd:type":"Descriptor"},$value:"1D-PTO Plan","ID":[{"attributes":{"wd:type":"Time_Off_Code"},"$value":"JR Sick"}]},"Comment":{"$value":"Wandabot created this request!"}}}};
              var args = `{"bsv:Enter_Time_Off_Data":{"bsv:Worker_Reference":{attributes:{"wd:Descriptor":"Colin Drake"},"bsv:ID":[{"attributes":{"wd:type":"Employee_ID"},"$value":"21035"}]},"bsv:Enter_Time_Off_Entry_Data":{"bsv:Date":{"$value":"2018-06-12"},"bsv:Requested":{"$value":"8"},"bsv:Time_Off_Reference":{attributes:{"Descriptor":"1D-PTO Plan"},"bsv:ID":[{"attributes":{"wd:type":"Time_Off_Code"},"$value":"JR Sick"}]},"bsv:Comment":{"$value":"Wandabot created this request!"}}}}`;
    
              var options = {
                  endpoint: 'https://wd2-impl-services1.workday.com/ccx/service/cpsg_dpt1/Absence_Management/v31.0'
              }
  
              soap.createClient(apiWSDL, options, function (err, client) {
  
                  var wsSecurity = new soap.WSSecurity(userId, password);
  
                  client.setSecurity(wsSecurity);
                  client.Enter_Time_Off(args, function (err, result) {
                 
                      if(result)
                      {
                          console.log("Result " + JSON.stringify(result));
                      }
                      else if(err)
                      {
                          console.log("Error " + JSON.stringify(err));
                      }
  
  
                      callback(result);
                  });
              });
  
          }
          catch (e) {
              console.log(e);
              res.status(500).json({ success: false });
          }
      }*/



}