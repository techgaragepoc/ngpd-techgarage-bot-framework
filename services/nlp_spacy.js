var request = require('request');



module.exports = {
    getIntentTree: function(message, callback){
        //console.log('http://localhost:5001/api/getPOS/workdaybot/' + message);
        request('http://104.210.214.28:5001/api/getPos/xbot/' + message, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var _entities = {
                            user_text: "",
                            intent : "",
                            noun : "",
                            verb : "",
                            adj : "",
                            entity: ""
                        };
                _entities.user_text = message;
                console.log("Message ",message);
                //console.log(body) // Print the google web page.
                resp =  JSON.parse(response.body).message;
                resp = resp.split("'t").join(" not");
                var Intents = JSON.parse(resp).Intents;
                console.log("Intents  " + JSON.stringify(resp));
                var intent = "";
                var _score = 0;
                Intents.forEach(function(element){
                    for(var key in element) {
                        var key_value = element[key];
                        if(key_value > 0.5 && key_value > _score){
                            intent = key;
                            _score = key_value;
                        }
                    }
                }, this);
                _entities.intent = intent;
                var Nouns = JSON.parse(resp).Nouns;
                Nouns.forEach(function(element) {
                    _entities.noun += _entities.noun == ""? element.value: " , " + element.value;
                }, this);
                var Verbs = JSON.parse(resp).Verbs;
                Verbs.forEach(function(element){
                     _entities.verb += _entities.verb == ""? element.value: " , " + element.value;
                }, this);
                var Adj = JSON.parse(resp).ADJ;
                Adj.forEach(function(element){
                     _entities.adj += _entities.adj == ""? element.value: " , " + element.value;
                }, this);
                var Entities = JSON.parse(resp).Entities;
                Entities.forEach(function(element){
                    _entities.entity += _entities.entity == ""? element.value : " , " + element.value;
                })


                
            }
            callback(_entities);
        })
    }
}