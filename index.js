var http            = require('http'),
    express         = require('express'),
    bodyParser 		= require('body-parser'),
    config          = require('./config'),
    context         = require('./context'),
    session         = require('express-session');
    Message         = require('./Message');
    timeOff = require('./services/workDay/TimeOff');
const MongoStore = require('connect-mongo')(session);

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.text());

/*app.use(session({
  cookieName: 'session',
  secret: 'random_string_goes_here',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
}));*/
app.use(session({
    store: new MongoStore({
      //url: 'mongodb://localhost/botframework_session',
      //url: 'mongodb://workdaybot:workdaybot@ds231360.mlab.com:31360/botframework_session',
      url: 'mongodb://workdaybot:workdaybot1@ds018538.mlab.com:18538/wanda',
      autoRemove: 'interval',
      autoRemoveInterval: 10 // In minutes. Default
    }),
    secret: 'somesecrettokenhere', 
    resave: false,
    saveUninitialized: true, 
    cookie: { secure: false, maxAge: 600000 }// Secure is Recommeneded, However it requires an HTTPS enabled website (SSL Certificate)
       
}))

//*** Allowing access to external request. Header initialization to enable api access from external applications
app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('access-Control-Allow-Origin', '*');
    next();
});



//*** Defining api router
var botbuilder = express.Router();
app.use('/api', botbuilder);

botbuilder.route('/messages/:userquery')
.get(function(req, res){
    if(req.session.userContext && req.params.userquery != "_start"){
        Message.messageHandler.processnewmessage(req, res);
    }else{
        if(req.query.data.userContext){
            context.init(req, res);
        }
        else
            res.status(500).jsonp({success : "false", message : "User session not initialized."})
    }
    
})


botbuilder.route('/starter/ams')
    .get(function (req, res) {
        try {
            var x = "";
            timeOff.GetTimeOffPlanBalances(21035, null, 'Network_Telecom_supervisory',function(result)
            {
                //console.log(result);
                x = JSON.stringify(result);
                
                res.status(200).json({ output: x });                                
            });                                                                    
        }
        catch (e) {
         //   console.log(" HEllo ");
          //  console.log(e);
            res.status(500).json({ success: false });
        }
    });

    botbuilder.route('/starter/holidaycal')
    .get(function (req, res) {
        try {
            var x = "";
            timeOff.GetHolidayCalendar("Holidays for the United States of America",function(result)
            {
                //console.log(result);
                x = result;
                res.status(200).json({ result: x });
                
                
            });                                                                    
        }
        catch (e) {
            console.log(" HEllo ");
            console.log(e);
            res.status(500).json({ success: false });
        }
    });
    
    botbuilder.route('/starter/time_off')
    .post(function (req, res) {
        try {
            var x = "";
            timeOff.AdjustTimeOff("Colin Drake","21035","2018-06-21","8","1D-PTO Plan","JR Sick",function(result)
            {
                //console.log(result);
                x = JSON.stringify(result);
                res.status(200).json({ result: x });
                
                
            });                                                                    
        }
        catch (e) {
            console.log(" HEllo ");
            console.log(e);
            res.status(500).json({ success: false });
        }
    });

    /* botbuilder.route('/starter/time_off')
    .post(function (req, res) {
        try {
            var request = require('request');
            let xml  = `<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bsv="urn:com.workday/bsvc">
            <x:Header>
                <bsv:Workday_Common_Header>
                    <bsv:Include_Reference_Descriptors_In_Response>false</bsv:Include_Reference_Descriptors_In_Response>
                </bsv:Workday_Common_Header>
                <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                    <wsse:UsernameToken>
                        <wsse:Username>ISU_Absence@cpsg_dpt1</wsse:Username>
                        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">PH\\\\_h3rP'98jz%4Y</wsse:Password>
                    </wsse:UsernameToken>
                </wsse:Security>
            </x:Header>
            <x:Body>
                                <bsv:Enter_Time_Off_Request bsv:version="v31.0">
                    <bsv:Enter_Time_Off_Data>
                        <bsv:Worker_Reference bsv:Descriptor="Colin Drake">                    
                            <bsv:ID bsv:type="Employee_ID">21035</bsv:ID>
                        </bsv:Worker_Reference>
                        <bsv:Enter_Time_Off_Entry_Data>
                            
                    <!--ENTER IN THE DATE HERE-->
                            <bsv:Date>2018-06-15</bsv:Date>
        
                            <bsv:Requested>8</bsv:Requested>
        
                           <bsv:Time_Off_Reference bsv:Descriptor="1D-PTO Plan">     	             
                              <bsv:ID bsv:type="Time_Off_Code">JR Sick</bsv:ID>
                           </bsv:Time_Off_Reference>
        
                    <!--ADD COMMENTS THAT SHOW THE CHATBOT DID THE WORK-->
                            <bsv:Comment>Wandabot created this request!</bsv:Comment>
                        </bsv:Enter_Time_Off_Entry_Data>
                    </bsv:Enter_Time_Off_Data>
                </bsv:Enter_Time_Off_Request>
            </x:Body>
        </x:Envelope>`;
        var options = {
            url: 'https://wd2-impl-services1.workday.com/ccx/service/cpsg_dpt1/Absence_Management/v31.0',
            method: 'POST',
            body: xml,
            headers: {
              'Content-Type':'text/xml;charset=utf-8'              
            }
          };

          let callback = (error, response, body) => {
            if (!error && response.statusCode == 200) {
              console.log('Raw result ', body);
              //var xml2js = require('xml2js');
              //var parser = new xml2js.Parser({explicitArray: false, trim: true});
              //parser.parseString(body, (err, result) => {
              //  console.log('JSON result', result);
              //});
            };
            console.log('E', response.statusCode, response.statusMessage);  
          };
          request(options, callback);
                                                                              
        }
        catch (e) {
            console.log(" HEllo ");
            console.log(e);
            res.status(500).json({ success: false });
        }
    });
 */
botbuilder.route('/starter/chat')
    .post(function (req, res) {
        try {

            var mongoose = require('mongoose');
            mongoose.connect('mongodb://127.0.0.1:27017/test');

            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error:'));
            db.once('open', function () {

                var kittySchema = mongoose.Schema({
                    name: String
                });

                var Kitten =  mongoose.models.Kitten  || mongoose.model('Kitten', kittySchema);

                var catInfo = req.body.CatInfo;
                console.log("New Cate " + catInfo.name);

                var cat = new Kitten({ name: catInfo.name });
                //console.log(silence.name); // 'Silence'

                //var fluffy = new Kitten({ name: 'fluffy' });

                // NOTE: methods must be added to the schema before compiling it with mongoose.model()
               

                cat.save(function (err, cat) {
                    if (err) return console.error(err);
                    //fluffy.speak();
                  });

                  Kitten.find(function (err, kittens) {
                    if (err) return console.error(err);
                    res.status(200).json(kittens);
                    //console.log(kittens);
                     

            });

        });


        }
        catch (e) {
            console.log(" HEllo ");
            console.log(e);
            res.status(500).json({ success: false });
        }
    });

var port = process.env.PORT || config.get('port');

var server = app.listen(port, () => {
  console.log('listening on port %s', port);
});

server.timeout = 360000;