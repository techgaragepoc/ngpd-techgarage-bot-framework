
module.exports = {
    dialogContext: {
    intent : "",
    dialog_name : "",
    current_step : "",
    next_step  : "",
    newconversation : true
},
    setDialog: function(req, intent, dialog_name, current_step, next_step, newconversation ){
        var dialogContext = {
            intent : "",
            dialog_name : "",
            current_step : "",
            next_step  : "",
            newconversation : true
        };
        dialogContext.intent = intent;
        dialogContext.dialog_name = dialog_name;
        dialogContext.current_step = current_step;
        dialogContext.next_step = next_step;
        dialogContext.newconversation = newconversation;
        req.session.dialogContext = dialogContext;
    }
}