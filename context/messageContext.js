var    msg_format = require('../Message/messageFormat');


module.exports = {
    Context: {
        text : "",
        sender : "",
        received_time : "",
        format : ""
    },
    init : function(req, msg){
        var message = {
            text : "",
            resp_text: "",
            sender : "",
            received_time : "",
            format : ""
        };
        message.text = msg;
        message.sender = req.session.userContext.user_name;
        message.received_time = Date.now();
        message.format = msg_format.text;
        req.session.messageContext = message;
        return message;
    }
}