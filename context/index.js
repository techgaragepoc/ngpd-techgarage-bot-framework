var config = require('../config'),
    Message = require('../Message'),
    messageContext = require('./messageContext'),
    userContext = require('./userContext'),
    botContext = require('./botContext'),
    dialogContext = require('./dialogContext'),
    appContext = require('./appContext');


module.exports = {
    userContext, 
    botContext, 
    dialogContext,
    messageContext,
    init : function(req, res){
        if(!req.query.data.userContext){
            res.status(500).jsonp({success: "false", message: "User context not provided"})
        }
        else if(!req.query.data.botContext){
            res.status(500).jsonp({success: "false", message: "Bot context not provided"})
        }
        else if(req.query.data.userContext && req.query.data.botContext){
            var userContext =  this.userContext;
            var _userContext = req.query.data.userContext;
            userContext.user_name = _userContext.user_name? _userContext.user_name: "";
            userContext.user_id = _userContext.user_id ? _userContext.user_id : "";
            userContext.location = _userContext.location ? _userContext.location : "";
            userContext.contact_no = _userContext.contact_no ? _userContext.contact_no : "";
            req.session.userContext = userContext;
            var botContext = this.botContext;
            var _botContext = req.query.data.botContext;
            botContext.bot_name = _botContext.bot_name || "";
            botContext.bot_id = _botContext.bot_id || "";
            req.session.botContext = botContext;
            var dialogContext = this.dialogContext.dialogContext;
            req.session.dialogContext = dialogContext;
            var _reply = Message.reply.SendInitialReply(req, Message.messageFormat.text);
            res.status(200).jsonp({success: "true", message : _reply});
        }
    }
};