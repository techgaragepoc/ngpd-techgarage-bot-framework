var msg_context = require('../context/messageContext');
    intentHandler = require('../intentHandler');


module.exports = {
    processnewmessage : function(req, res){
        message = msg_context.init(req,req.params.userquery);
        intentHandler.intentProcessor.processIntent(req, res, message);

    },
    captureConversation : function(req, message){
        req.session.conversation = req.session.conversation || "";
        req.session.conversation += req.session.userContext.user_name + ': ' + req.session.messageContext.text + '</br>';
        req.session.conversation += req.session.botContext.bot_name + ': ' + message + '</br>';
    }
}