var config = require('../config');
var util = require('util');
var msg_handler = require('../Message/messageHandler');


module.exports = {
    SendReply : function(req, res, message, format){
        var reply;
        switch (format){
            case "text":
                reply = message;
                break;
            default:
                reply = message;
        }
        msg_handler.captureConversation(req, message);
        console.log(req.session.conversation);
        res.status(200).jsonp({success : "true", message : reply}) ;
    },
    SendInitialReply : function(req, format){
        var reply;
        switch (format){
            case "text": 
                reply = util.format(config.get( 'dialog_initial_message'), req.session.userContext.user_name);
                break;
            default:
                reply = "";
            }
         return reply;
    }
};